---
layout: markdown_page
title: "VUL.4.01 - Enterprise Antivirus Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# VUL.4.01 - Enterprise Antivirus

## Control Statement

If applicable, GitLab has managed enterprise antivirus deployments and ensures the following:

* Signature definitions are updated
* Full scans are performed quarterly and real-time scans are enabled
* Alerts are reviewed and resolved by authorized personnel

## Context

This control outlines the components of a successfully deployed antivirus program which helps add another layer of risk mitigation to the GitLab environment. The applicability in this control is left vague since we have to apply some reason to this control. We know there are some systems that either aren't possible to install antivirus software on or systems that wouldn't have any risk reduced by installing antivirus software. The intent of this control is to install this software anywhere it is feasible to and not to only where it is convenient.

## Scope

This control applies to all systems within our production environment. The production environment includes all endpoints and cloud assets used in hosting GitLab.com and its subdomains. This may include third-party systems that support the business of GitLab.com.


## Ownership

* gitlab.com and live production environments
  * Security Operations - responsible for vulnerability management scanning tools
  * Infrastructure - responsible for addressing the scan findings within SLA

* Laptops
  * Security Management- responsible for enforcement of endpoint management
  * IT-Ops - responsible for rolling out endpoint management tool, monitoring tool, reporting on non-compliance devices - trigger an alert

## Guidance

Any production systems we are not installing antivirus software on should have a documented justification for why it isn't applicable. It is fine to have different antivirus software securing different systems, but the more different solutions we use, the more complexity we introduce into the maintenance of this control.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Enterprise Antivirus control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/942).

## Framework Mapping

* ISO
  * A.12.2.1
* SOC2 CC
  * CC6.8
  * CC7.1
* PCI
  * 5.1
  * 5.1.1
  * 5.1.2
  * 5.2
  * 6.2
