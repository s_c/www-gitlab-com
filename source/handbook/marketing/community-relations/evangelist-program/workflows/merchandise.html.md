---
layout: markdown_page
title: "Merchandise workflow"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

## Workflow

## Best practices

### Choosing a vendor

As a general rule, consider using [Stickermule](https://www.stickermule.com) for sending stickers, since the Printfection inventory is limited. If Stickermule doesn't work for you, then use Printfection instead.

If the merch shipment includes:
* only stickers, always use Stickermule
* a small number of items (depending on Printfection inventory), use Printfection
* a large amount of stickers and other merch, consider using both Stickermule and Printfection

<i class="fas fa-hand-point-right" aria-hidden="true" style="color: rgb(138, 109, 59)
;"></i> Always check the Printfection inventory and item availability before sending.
{: .alert .alert-warning}

### External Shopify orders

All the orders received via [shop.gitlab.com](https://shop.gitlab.com/) are automatically forwarded from Shopify to Printfection via the Printfection-Shopify Zapier integration. On Printfection we manually process the orders and fulfill the shipments. You can always check the status of the orders in the collection tab in Printfection.

#### Fulfillment in Shopify

Since the orders from Shopify are auto-forwarded and fulfilled in Printfection, the only thing left to do is to fulfill the orders in Shopify as well. This is only to get the notification sent to the customer.

1. Open the Shopify **Orders** page.
2. Open the Printfection's Shopify store orders collection.
3. Compare if all the **Unfulfilled** orders from Shopify are listed in Printfection.
4. Mark the **Unfulfilled** orders in Shopify.
5. Click drop-down **Actions** button.
6. Choose **Fulfill order** option.

#### Canceling any external order on Shopify and Printfection

You can always cancel the pending/processing orders. All the orders including the orders via Shopify Collections using Discount Codes can be canceled.

1. From your Printfection home page go to the **Shopify store orders** collection.
2. Under the **Manage** tab, search for the order you want to cancel.
3. Click **Delete order** button.
4. You are done. The order won't be fulfilled by Printfection.

Please note that you should always change the status of the orders in Shopify as well.

1. From your Shopify admin page go to the **Orders** page
2. Click on the order you want to change.
3. Press **More Actions** button in order to see the drop menu options.
4. Select **Cancel order** option.

#### Printfection fulfillment notifications

The updates about the orders are sent to both <merch@gitlab.com> and the customer's email address. You can find those messages in the `Merchandise` view on [GitLab Community Zendesk instance](https://gitlab-community.zendesk.com/).

#### Tracking IDs

The Tracking ID is usually assigned by Printfection 2-3 days after the order is received. Sometimes, users may request their Tracking ID.

Please follow these steps if the user requests the Tracking ID:
1. Look for the requester's **name/email**.
2. Go to the Printfection **Reports** page.
3. Search for the order using the name/email.
4. Copy the **Tracking ID**.
5. Confirm that order's details match the requester. You can double check this via Shopify:
    * Search for the same order/person in Shopify
    * Compare if the **items**, **full name**, **email** and the **dates** are correct
6. Email the **Tracking ID** or the full **Tracking Link** to the requester.
    * You can always open the full **Tracking Link** by clicking on the **Tracking ID**.
7. (Optional step) Assign the **Tracking ID** to the requester's order in Shopify:
    * Find the order in Shopify
    * If you already fulfilled the order in Shopify, click **Add tracking** button and paste the ID.
    * If the order is unfulfilled in Shopify, mark it as **fulfilled** and then add the **Tracking ID**.

#### Sending merch via Printfection 

1. Go to any collection on Prinfection
2. Click on **Manage** tab.
3. Press the **Add order** button
4. Pick the items and enter the **quantity**.
5. Important: click **Save order** button.
6. Input the shipping details.
7. Imporant: click **Save order** button
8. You are ready to click the **Place the order** button

### Add or remove the products from Shopify

#### Adding items

1. Gather item inventory data - contact the product's vendor
1. Log in to Shopify
1. Open the products page
   - Click the Add Product button
   - Fill out information about the item
   - If you don't have the information for the description, please ask/search for it and be careful - the info could be sensitive
   - Image is important, contact the product vendor for the high-rez photo.
   - Fill out the price for the item
   - Select "Shopify tracks this product's inventory"
   - Enter the weight of the product if that info is available.
   - Before saving the product, please check search engine listing preview

<i class="fas fa-info-circle" aria-hidden="true" style="color: rgb(49, 112, 143)
;"></i> For more information, see this [official guide](https://help.shopify.com/en/manual/products/add-update-products)
{: .alert .alert-info}

#### Removing items

1. Log in to Shopify
1. Open the Products page
   - Click on the product you want to remove
   - Scroll to the bottom of the page where you can find the delete button

<i class="fas fa-info-circle" aria-hidden="true" style="color: rgb(49, 112, 143)
;"></i> For more information, see this [official guide](http://shopifynation.com/shopify-tutorials/delete-products-variants-shopify/)
{: .alert .alert-info}

## Handling swag

### Swag self-service (for advocates)

#### Collections 

Printfection's Collection campaign is a way to easily collect orders, review them, and then place them all at once. You can manually key-in orders, import orders from a CSV file, or allow other parties to place orders through a hosted landing page.

##### Create a new Collection campaign

1. Go to Campaigns tab then click Collections 
2. Click the **+COLLECTION CAMPAIGN** button. 
3. Enter the name,
4. Select the option that turns on External Ordering (this option will provide you a link that allows users to place orders for this campaign).
5. Press **CREATE CAMPAIGN** button.
6. Go to the **Items** tab in the navigation menu.
7. Click **Add Items to Campaign** and simply choose the items you want to offer in your Collection.
8. Go to **Settings** in the navigation menu and update the GitLab branding (see the existing givaway settings). You'll also need to specify a payment method on this page.
9. Turn the campaign from **Paused** to **Running** in the top right navigation menu. And that's it, you're ready to give some swag!

Last, but not least, you'll want to review the orders from the Manage page within your Collection campaign. Here you can change, update, or remove orders. Review your totals, fulfillment cost, and other details.  When you're ready hit Place Orders and place them all at once!


#### Giveaways

Printfection has the giveaway campaigns which allow us to send a link to our customers and let Printfection handle the rest.

The giveaway campaign works like this:  

1. Choose your swag offerings & brand your redemption page
2. Send out giveaway links
3. Customers enter their name and address
4. **Manually** fulfill the order in Printfection.

##### Create a new Giveaway campaign
 
1. Go to Campaigns tab then click Giveaways 
2. Click the **+ GIVEAWAY CAMPAIGN** button. 
3. Enter the name and press **CREATE CAMPAIGN** button.
4. Go to the **Items** tab in the navigation menu.
5. Click **Add Items to Campaign** and simply choose the items you want to offer in your Giveaway.
6. Click the **Manage** in the navigation menu and choose how many initial links you want to giveaway (you can always add more later).
7. Go to **Settings** in the navigation menu and update the GitLab branding (see the existing givaway settings). You'll also need to specify a payment method on this page.
8. Turn the campaign from **Paused** to **Running** in the top right navigation menu. And that's it, you're ready to give some swag!

Note: Once a redemption is complete you will have the option to cancel it from the 'Recipients & Redemption' section at the bottom of the 'Overview' page. Just use the 'Cancel' button next to the order. This cancelation option is only available until the order is processed, you'll want to review orders same-day or earlier if you want to cancel them.

### MVP Appreciation Gifts

Each 22nd of the month is a release day - every release we pick a Most Valuable Person and thank them for their contributions. We send them some GitLab swag as a thank you (e.g. a hoodie, socks, and a handmade tanuki). There's also the option of sending personalized swag - see [custom swag providers](#good-custom-swag-providers).

1. Determine MVP after merge window closes, see `#release-post` channel
1. Find MVP's contact information
  * An email address is usually stored in git commit data
  * A user might have email or twitter info on their profile
1. Congratulate the MVP via email, ask for their shipping address, as well as any other relevant information (e.g. shirt size)
1. Investigate the MVP's interests
  * If the MVP doesn't have a notable presence on social media, you may choose to ask them directly or send GitLab swag instead
1. Choose a suitable gift (up to 200$ USD)
1. Write a kind thank you message
1. Send the gift
  * The MVP should ideally have the gift 48h before the post goes live, though shipping to people outside the United States can take longer and usually won't make it in time
1. Verify shipment status
  * Make sure that it was sent
  * Make sure that it arrived
1. Mention the MVP gift in the release post
  * Make sure there's a picture of the gift in the release post if it's available

### Handling #swag channel and <merch@gitlab.com> requests

#### Internal GitLab Merchandise request

Everyone can request merch in the #swag Slack Channel or email the request to <merch@gitlab.com>.

We can ship the package to any location:
* If you are attending an event, you can request the merch to arrive at your place.
* If you want to send gift to the customer, user or contributor - you can request to arrive at your location so you can hand it personally or we can send it directly to the recipient.

Please include the following info for any type of request:
* What merchandise items do you need.
* The amount of merch needed - feel free to ask the Community Advocate swag expert for the suggestion if you are not sure.
* The merchandise shipping address and contact phone number - feel free to continue the conversation via DM if you don't want to share this info publicly.
* If you are giving away a swag gift to a contributor please include the URL to a blog post, Tweet or the contribution.

Notes: we recommend that you request merchandise at least 4 weeks in advance for us to be able to accommodate your request. However,
* If your request is urgent, please reach out to the swag expert and find out if the fast shipping option is available.
* Feel free to schedule a Zoom call with the swag expert to discuss, create and place the order.
* The swag expert should send the notification when the order is placed.
* Tracking IDs are available once the package is shipped which usually takes 1-2 days.
* In order to keep the orders transparent, please do not send requests via direct messages, but use the #swag channel or <merch@gitlab.com>.

#### Community Advocates
* Make sure you regularly check the #swag channel and the Merchandise view in the Zendesk.
* If the recipient is  a contributor, user or customer make sure you reach out to the recipient via <community@gitlab.com>:
  * Thank them for their work/support
  * Gather the missing info needed for fulfilling the swag dropship if needed
* Fulfill the shipment in Printfection:

<i class="fas fa-hand-point-right" aria-hidden="true" style="color: rgb(138, 109, 59);"></i> Please bear in mind the [list of countries we do not do business in](/handbook/sales/#export-control-classification-and-countries-we-do-not-do-business-in).
{: .alert .alert-warning}

### Delayed and lost merchandise shipments

From time to time it may happen that the package never arrives to the customers. Customers usually complain via <merch@gitlab.com>, however, keep an eye on Twitter, the #swag Slack channel and other related threads.

Please check if the package is still in transport using the tracking ID and reach out to the customer with brief details.

If the package has been in transport over 2-3 weeks, consider apologizing and refunding the 20% of the whole order using Shopify's refund option:
1. Login to Shopify.
2. Search the order by name/email/orderID.
3. Select the order.
4. Select the "Refund items" option.
5. On the right part of the page, you have the fields to enter the custom value and reason for a refund.
6. If you are not sure how to calculate the 20%, multiply 0.2 with the whole amount and that's the exact value.
7. Use "Reason for refund" field and write the appropriate message explaining that we are refunding 20% of the whole amount due to delayed shipping and press the Refund button.
8. Don't forget to apologize to the customer using the original thread (e.g. respond via the original Zendesk ticket) and offer any other assistance if needed.

If the customer complains that the package never arrived and the package status is "completed" or "delivered", consider the following options:
1. Reach out to the vendor (<support@printfection.com> or <help@stickermule.com>) and ask if they have information about that specific order.
2. If the package has been returned or lost, consider asking them to resend it.
3. If the vendor doesn't resend the package, do it manually asap.
4. Always consider refunding the whole order amount to the customer.
5. Since we care about our community and customers, feel free to include extra item/s of your choice, create a coupon code for an apology or any other idea. In this case, the main goal is to make the customer happy.
6. Don't forget to coordinate everything with the customer (use <merch@gitlab.com> for conversation), apologize and find out if there's any other thing we could do for them.
