---
layout: job_family_page
title: "Director, Contracts and Legal Operations"
---

Director, Contracts and Legal Operations

The Director, Contracts and Legal Operations is responsible for managing the contract management team. At the direction of the VP of Legal, the Director, Contracts and Legal Operations will standardize the contracting process and will be part of the tiered escalation process. This is a remote role.

## Responsibilities

Build a cohesive contracting team and implement efficient processes in support of our customer sales organization
Work closely with members of the Legal team to develop and improve applicable standardized forms, processes, and procedures
Understand the Company and products in order to identify risks, develop solutions, mitigation and negotiation strategies
Draft and negotiate a wide range of contracts including services, consulting, marketing, licensing, non-disclosure, data privacy, and other commercial and technology related agreements

## Requirements for candidate

* Minimum of 7 years of contracts drafting, reviewing and negotiation experience at a software technology company
* BA/BS required. JD, paralegal certificate, or equivalent work experience preferred.
* Experience successfully managing and growing a team.
* Expertise in drafting and negotiating a wide variety of commercial, licensing and other technology-related agreements.
* Previous experience creating and implementing business processes to make the legal operations more efficient.
* Experience creating contract negotiation playbooks and other tools to standardize contract negotiations.
* Experience with contract management databases, Conga Contracts, ContractWorks a plus.
* Proactive, dynamic and result driven individual with strong attention to detail.
* Outstanding interpersonal skills, the ability to interface effectively with all business functions throughout the organization.
* Enthusiasm and "self-starter" qualities enabling him or her to manage responsibilities with an appropriate sense of urgency; the ability to function effectively and efficiently in a fast-paced & dynamic environment.
* Superior analytical ability, project management experience, and communication skills
* Ability to manage internal customer priorities and needs.
* Previous experience in a Global Start-up and remote first environment would be ideal.
* Successful completion of a background check.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our team page.

* Selected candidates will be invited to schedule a 45 min [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our VP of Legal.
* Candidates might at this point be invited to schedule with an additional team members
* Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
