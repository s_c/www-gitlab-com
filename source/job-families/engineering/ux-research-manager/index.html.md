---
layout: job_family_page
title: "UX Research Manager"
---

## UX Management Roles at GitLab

Managers in the UX Research department at GitLab see the team as their product. While they are credible as researchers and know the details of what UX Researchers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of UX Research commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

### UX Research Manager

The User Experience (UX) Research Manager reports to the Director of UX, and UX Researchers report to the UX Research Manager.

#### Responsibilities

* Hire a world class team of UX Researchers.
* Hold regular 1:1’s with team members.
* Develop and drive a research strategy for GitLab.
* Operationalize UX Research in order to reduce inefficiencies, make UX Research scalable via repeated processes, ready-to-apply methods and templates.
* Identify and select the processes and tools needed to mature our UX Research practices.
* Share research expertise and knowledge with others, and encourage the wider organisation to actively participate in UX Research, to improve organisational capability.
* Lead and mentor UX Researchers, support their career development.
* Evangelize research. Share user insights with the broader organisation and externally in creative ways to increase empathy. 
* Partner with colleagues in Product Design, Product Management, Product Marketing, Engineering, Sales and Support.

#### Requirements

* Expert-level knowledge in the field of UX Research with at least 5 years of experience in hands-on research.
* A minimum of 3 years of experience managing UX Researchers.
* Prior experience of building a UX Research team from the ground up.
* Outstanding communicator - both verbally and in writing.
* Strong collaboration skills.
* A proven record of driving change through UX Research.
* Passion for the field of UX Research.
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)

**NOTE** In the compensation calculator below, fill in "Manager" in the `Level` field for this role.

## Performance indicators
* [Hiring Actual vs Plan](/handbook/engineering/ux/performance-indicators/#hiring-actual-vs-plan)
* [Diversity](/handbook/engineering/ux/performance-indicators/#diversity)
* [Handbook Update Frequency](/handbook/engineering/ux/performance-indicators/#handbook-update-frequency)
* [Team Member Retention](/handbook/engineering/ux/performance-indicators/#team-member-retention)

#### Interview Process

- [Screening call](/handbook/hiring/#screening-call) with a recruiter
- Interview with a UX Researcher (report)
- Interview with a UX Director (manager)
- Interview with a UX Manager (peer)
- Interview with VP of Engineering
