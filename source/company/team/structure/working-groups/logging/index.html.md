---
layout: markdown_page
title: "Logging Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property     | Value |
|--------------|-------|
| Date Created | September 9, 2019 |
| Date Ended   | TBD |
| Slack        | [#wg_log-aggregation](https://gitlab.slack.com/messages/CGR0T1C6P) (only accessible from within the company) |
| Google Doc   | [Logging Working Group](https://docs.google.com/document/d/1Trzy-mbxsoq8UP6WtAnLCfYEXim1KdiBB0DftCaDOfY/edit?usp=sharing) (only accessible from within the company) |
| Issue Label | WG-Logging (gitlab-com/-org) |

## Business Goal
1. One Group to Own Logging Infrastructure, Company-Wide
  * Use as few loggins systems as possible to achieve these goals (dev teams use ELK, security team uses a different ELK, HAProxy logs are stored in BigQuery, etc)
1. Define and Maintain Logging Infrastructure Stores at Different Retentions
  * For each type of log, it should be easy to determine which component of the logging infrastructure will be used to store it
1. Secure Logging Infrastructure Stores with Different Access Levels
1. Identify All Log Sources and Assign Retention, Access Control Needs, and Owner(s)
  * Determine all existing log sources in infra, dev projects, and security team and assign retention needs, access control needs, and owners
1. Create Formatting, Process, and Standards Guidelines for Logging
  * Create a handbook article on log formatting, the process for adding new log sources, and other standards to help teams with their logging needs


## Exit Criteria

TBD


## First Steps

* Create spike issue for how to begin to unite delke and GitLab.com elastic search instances
* Create spike issue for what technologies to use for different retentions (elastics, bigquery, hotwarm)
* Analyze and document the locations, retention, and variety of production logs as they exist today, including analysis of the data classification and access controls
* Generate the people & responsibilities table for all of these services/features of GitLab.com
* Starting a document on this; begin creating the process for log change requests for other teams to handle


## Other Investigations

### What do other companies do?

* Jobs and files in ParK in AWS (wife just started there)
* Look into Presto (Facebook Distributed SQL)
* HotWarm ElasticSearch



### LabKit

LabKit is an application logging library Andrew Newdigate invented to help structure and standardize logging (similar to Graphite pings) throughout the Ruby and Go code bases


### Where do logs go today?

* Unstructured logs (redis, etc) sent to GCS and Stackdriver
* Structured sent to ELK
* Large structured logs sent to BigQuery (via Stackdriver)


## Related Issues

* [Kickstart the GitLab.com Working Group](https://docs.google.com/document/d/1PRJtvAPHOMF1d152vGMpPWxRZNu12vCxvlnVgZud47c/edit#heading=h.mm4gsg9so2kc)
* [Add Three More 3rd Party Service Audit Ingestions](https://gitlab.com/gitlab-com/gl-security/engineering/issues/607)
* [Release GitLab Runner Metrics Collection to Production](https://gitlab.com/gitlab-com/gl-security/engineering/issues/604)
* [Augment GitLab Runner Metrics Collection w/Suricata Metrics](https://gitlab.com/gitlab-com/gl-security/engineering/issues/605)
* [Rails production logs cannot be fully loaded into BigQuery without pre-processing](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/7564)


## Roles and Responsibilities

| Working Group Role  | Person           | Title                                  |
|---------------------|------------------|----------------------------------------|
| Facilitator         | Alex Groleau     | Security Software Engineer, Automation |
| Exec Sponsor        | Kathy Wang       | Senior Director of Security            |
| Member              | Stan Hu          | Engineering Fellow                     |
| Infrastructure Lead | Andrew Newdigate | Staff Engineer, Infrastructure         |
| Member              | Ethan Urie       | Senior Backend Engineer, Security      |
| Member              | Antony Saba      | Senior Threat Intelligence Engineer    |
| Member              | Tomasz Mazukin   | Backend Engineer, Verify               |
| Member              | Jayson Salazar   | Security Engineer, Security Operations |
| Member              | Paul Harrison    | Senior Security Engineer               |
| Member              | Nik Sarosy       | Senior Security Analyst, Compliance    |
