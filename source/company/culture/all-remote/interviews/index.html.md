---
layout: markdown_page
title: "All-Remote interview questions"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

On this page, we're curating a list of questions that we at GitLab appreciate hearing answers to from others in remote organizations.

## General remote-work interview questions

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/IFBj9KQSQXA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

GitLab seeks to learn from others who are embracing remote work. This list of questions is directed at other peers working within a remote organization. Some are aimed at executives and hiring managers. In the video above, found on the [GitLab Unfiltered YouTube channel](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A), Sid asks the below questions to InVision Chief People Office Mark Frein.

We are working on a submission process for remote companies who want to engage in interviews with GitLab to share their perspective on these questions and more — stay tuned! 

1. What’s driving society’s appetite for workplace flexibility and remote opportunities? 
1. What qualities are important to look for in new hires as you scale a remote organization?
1. How does all-remote contribute to people being themselves/vulnerable?
    What benefits have you seen manifest from this?
1. Do you need special leadership qualities to lead an all-remote team, or does what you learn leading traditional teams transfer over? 
1. To what degree can remote comfortability be learned?
    - Is it easier to learn/adapt than learning to function in a co-located organization?
1. Do you feel that all-remote organizations contribute to solving cost-of-living, traffic and housing crises in cities like San Francisco and Los Angeles? 
1. Do you feel that all-remote organizations are more inclusive by their nature?
    - Is all-remote a mechanism to tackle ageism in the workplace? 
1. What’s the impact of bringing skilled jobs to rural communities and underserved countries?
    - What kind of GDP-altering potential is there if all-remote itself is scaled?
1. Can existing co-located organizations transition to all-remote?
1. What encouragement would you give new startups today to structure their company as an all-remote entity? 
1. What types of businesses are best suited for all-remote?
1. Why do employees apply to work in an all-remote culture?
1. What’s the most important communication channel or practice that you use internally to keep your remote team connected and engaged?
1. What advantages do remote organizations have when it comes to building and sustaining their company culture?
1. What are some things you’d encourage newly remote employees to do?
    - Join a [WiFi Tribe](https://wifitribe.co/) digital nomad chapter?
    - Consider relocating (temporarily or permanently)?
    - Working in new time zones?
1. How do you weave in-person touch points or communal interactions into your organization? 

## Interview guests and answers

Below is an archive of past interview guests, answering the above questions and discussing remote work with GitLab team members.

### Pick Your Brain interviews

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/WBf_DA0FF9k" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

An archive of Pick Your Brain interviews are below. We've also created a [Pick Your Brain Playlist](https://www.youtube.com/playlist?list=PLFGfElNsQthafBVmoPPVMvBc_Gg2nsyQb) on [GitLab's YouTube channel](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg).

Learn more about scheduling a GitLab [Pick Your Brain series](/handbook/eba/#pick-your-brain-meetings).

- [GitLab CEO Sid Sijbrandij and InVision Chief People Officer Mark Frein](/2019/07/31/pyb-all-remote-mark-frein/)
- [GitLab CEO Sid Sijbrandij and FormAssembly CEO Cedric Savarese](/2017/08/11/pick-your-brain-interview-cedric-savarese/)
- [GitLab CEO Sid Sijbrandij and Polymail Co-founder and CEO Brandon Foo](/2017/06/02/pick-your-brain-interview-brandon-foo/)
- [GitLab CEO Sid Sijbrandij and Stitch Co-founder and CEO Jake Stein](/2017/08/18/pick-your-brain-interview-jake-stein/)
- [GitLab CEO Sid Sijbrandij and SaaS.CEO Founder Vincent Jong](/2018/01/26/pick-your-brain-interview-vincent-jong/)
- [GitLab CEO Sid Sijbrandij and Crazy Wisdom Podcast host Stewart Alsop III](https://www.youtube.com/watch?v=23XIx6n9SsQ)
- [GitLab CEO Sid Sijbrandij and Outklip Founder Sunil Kowlgi](/2019/04/18/lessons-on-building-a-distributed-company/)
- [GitLab CEO Sid Sijbrandij and Zapier's Mike Knoop and Noah Manger](/2018/01/08/zapier-pick-your-brain-interview/)
- [GitLab CEO Sid Sijbrandij and FineTune CTO Kwan Lee](/2017/09/15/pick-your-brain-interview-kwan-lee/)
- [GitLab CEO Sid Sijbrandij and Buffer CEO Joel Gascoigne](/2017/03/14/buffer-and-gitlab-ceos-talk-transparency/)
- [GitLab CEO Sid Sijbrandij and leadership psychologist Banu Hantal](/2019/06/21/cofounder-relations/)
- [GitLab CEO Sid Sijbrandij and Slab co-founder Jason Chen](/2016/07/14/building-an-open-source-company-interview-with-gitlabs-ceo/)

----

Return to the main [all-remote page](/company/culture/all-remote/).