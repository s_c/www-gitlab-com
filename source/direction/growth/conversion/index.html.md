---
layout: markdown_page
title: Product Vision - Conversion
---
 
### Overview:
The Growth Conversion Team at GitLab focuses on running experiments to increase the rate at which free accounts upgrade to become paying customers. Today, it can be challenging for a free user to understand the features that are available within each tier of GitLab when navigating within the app. We want to make it as easy as possible for teams to collaborate together on the features that are best for them and to explore purchasing on their own terms whether that’s starting a trial, speaking with someone at GitLab or upgrading on their own.
 
**Mission:**
* To continually improve the user experience when interacting with locked features, limits and trials.
 
**Team:**
 
Product Manager: [Sam Awezec](https://about.gitlab.com/company/team/#sam-awezec) | Engineering Manager: [Phil Calder (Interim)](https://about.gitlab.com/company/team/#pcalder) | UX Manager: [Jacki Bauer](https://about.gitlab.com/company/team/#jackib) | Product Designer: [Kevin Comoli](https://about.gitlab.com/company/team/#kcomoli) | Full Stack Engineer: [Alper Akgun](https://about.gitlab.com/company/team/#a_akgun) | Full Stack Engineer: TBH
 
**Conversion KPI:**
* Free to paid signup ARR growth rate
* Definition:  (Free to paid signup ARR in the current period - free to paid signup revenue in the prior period) / free to paid signup ARR in the prior period
 
**Supporting performance indicators:**
 
* New ARPU
* IACV
* Free to paid conversion rate (free user upgrades / free user signups in period)
* Free trial conversion rate
* Product driven revenue by channel (e-commerce, sales rep assisted) & funnel
* E-commerce and sales rep assisted will track our core funnel efficiency. Funnel will allow us to understand the value of core actions taken within the app. We will define these actions to start - they may be ‘upgrade now’ ‘talk to sales’ ‘start a trial’ ‘request access’. We then take the total occurrences and multiply it by the close rate and ASP to understand the value of the action in-app.
**Other reports we’re going to be monitoring**
In-app “upgrade now” buttons
* In-app “upgrade now” button selection to customers app purchase and ASP
* In-app “upgrade now” button selection to sales representative purchase and ASP
Started trial after GitLab.com sign up
* Trial to customers app purchase and ASP
* Trial to sales representative purchase and ASP
 
GitLab.com influenced revenue
User
* Time from GitLab.com user sign up to .com purchase (customers app or through sales representative)
* Breakdown of percent of .com purchases from customers app vs sales representative
Group
* Time from GitLab.com group sign up to .com purchase  (customers app or through sales representative)
* Group ASP
* Breakdown of percent of .com purchases from customers app vs sales representative
 
GitLab self-hosted cross over revenue
User
* Time from GitLab.com user sign up to .com purchase (customers app or through sales representative)
* User ASP
* Breakdown of percent of .com purchases from customers app vs sales representative
Instance
* Time from GitLab.com group sign up to .com purchase  (customers app or through sales representative)
* Instance ASP
* Breakdown of percent of .com purchases from customers app vs sales representative
 
### Problems to solve
Have you experienced an issue working a free user attempting to upgrade or do you have ideas on how we can improve the experience? We’d love to hear from you!
 
**Here are a few of the areas we’re already digging into:**
 
* Creating a universal module in the app when a user reaches a feature that they currently do not have access to due to it being a paid feature. The module will allow us to experiment with educating users about premium features and tiers along with providing options for next steps. For example, if someone is an admin which options do they prefer upgrade now, starting a trial, reading documentation. If someone is not an admin how can we best connect them with their admin if they think their company would benefit from the paid feature/tier.
* The upgrade experience once a free account starts a trial.
 
* We're documenting what paywall states exist today and if we'd like to create any net new ones.
 
### Our approach
As we build out our backlog of experiments we will utilize the ICE framework (impact, confidence, effort) to ensure we’re best utilizing our time and resources.
 
### Maturity
The team is brand new as of August  2019, our goal is to build out experimentation framework and the backlog of experiment ideas so we can become a well-oiled machine in the months to come.
 
### Helpful links
[GitLab Growth project](https://gitlab.com/gitlab-org/growth)
 
[KPIs & Performance Indicators](https://about.gitlab.com/handbook/product/metrics/)
 
Reports & Dashboards (these will be linked once they are created)