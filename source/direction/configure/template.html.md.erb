---
layout: markdown_page
title: "Product Vision - Configure"
---

- TOC
{:toc}

This is the product vision for Configure.

- See a high-level
  [roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=✓&state=opened&label_name[]=devops%3A%3Aconfigure).

## Overview

The Configure stage deals with the configuration and operation of applications
and infrastructure, including Auto DevOps, Kubernetes integration, and ChatOps.
We aim to make complex tasks (such as standing up new environments) fast and
easy as well as providing operators all the necessary tools to execute their
day-to-day actions upon their infrastructure.

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/imOtGmDJDCE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Background

In keeping with our [single application](https://about.gitlab.com/handbook/product/single-application/) promise, we want GitLab to be a robust, best-of-breed tool for operators as much as it is for developers. Our vision is that operators will use GitLab as their main day-to-day tool for provisioning, configuring, testing, and decomissioning infrastructure. 

### Challenges

We understand GitLab will play side-by-side with existing tools onto which teams may already have invested considerable time and money. This means we will need to extend GitLab's Configure features to have rich interactions through both the GUI and API. To provide the best experience possible we must identify best-of-breed tools in this segment that share our open source DNA and provide seamless integration.

GitLab is coming onto many shops (of every scale) that have adopted Kubernetes prior to beginning their use of our application. We must adapt to the most common set of use cases where existing cluster information can be plugged in to GitLab and users can see immediate value by using our integration. This includes enterprise and startup Kubernetes use.

Our Kubernetes integration currently operates under the assumption of `cluster-admin` privilege. While this is fine for the majority of use cases, there is a large minority that operates strictly on least-privilege security principle and will allow single-namespace access only and deny cluster-wide access. We want to provide the same rich integration we provide today for this further restricted use case.


### Opportunities

* **IT operations skills gap:** DevOps transformations are hampered by a [lack of IT operations skills (i)](https://docs.google.com/presentation/d/1tt6-XqnhCmsx8Jfy39hit8OgggeaFUUxxU7TnNsG4Qk/edit#slide=id.p19). Most organizations have taken to creating [infrastructure platform teams](https://hackernoon.com/how-to-build-a-platform-team-now-the-secrets-to-successful-engineering-8a9b6a4d2c8) to [minimize the amount of operations expertise required by DevOps team members (i)](https://drive.google.com/file/d/1GexcUo4FzmhmV30tnjn7x3hyk1plEK4-/view).  
* **Clear winner in Kubernetes:** Driven by software-defined infrastructure, cost management, and resiliency, organizations [are flocking to cloud-native application architectures (i)](https://drive.google.com/file/d/1ZAqTIiSfpHKyVFpgMnJoOBnyCQ-aF3ej/view). Kubernetes is the clear winner in container orchestration platforms. [Gartner recommends (i)](https://drive.google.com/file/d/19MZsKTc8tvAAFQEWuxDGoTW1f_kCZ6GI/view) organizations choose vendors that support open-source software projects in the cloud native ecosystem ([CNCF](https://www.cncf.io/)). 

## Auto DevOps

Our vision for “[Auto DevOps](https://www.youtube.com/watch?v=KGrJguM361c)” is
to leverage our [single application](/handbook/product/single-application/) to
assist users in every phase of the development and delivery process,
implementing automatic tasks that can be customized and refined to get the best
fit for their needs.

With the dramatic increase in the number of projects being managed by software
teams (especially with the rise of micro-services), it's no longer enough to
just craft your code. In addition, you must consider all of the other aspects
that will make your project successful, such as tests, quality, security,
logging, monitoring, etc. It's no longer acceptable to add these things only
when they are needed, or when the project becomes popular, or when there's a
problem to address; on the contrary, all of these things should be available at
inception.

e.g. “auto CI” to compile and test software based on best practices for the most
common languages and frameworks, “auto review” with the help of automatic
analysis tools like Code Climate, “auto deploy” based on Review Apps and
incremental rollouts on Kubernetes clusters, and “auto metrics” to collect
statistical data from all the previous steps in order to guarantee performances
and optimization of the whole process. Dependencies and artifacts will be
first-class citizens in this world: everything must be fully reproducible at any
given time, and fully connected as part of the great GitLab experience.

[Watch the video explaining our vision on Auto DevOps](https://www.youtube.com/watch?v=KGrJguM361c)

[Learn more](/product/auto-devops/) • [Documentation](https://docs.gitlab.com/ee/topics/autodevops/) • [Vision](/direction/configure/auto_devops/)

## Kubernetes Configuration

Configuring and managing your Kubernetes clusters can be a complex, time-consuming task. 
We aim to provide a simple way for users to configure their clusters within GitLab; tasks 
such as scaling, adding, and deleting clusters become simple, single-click events.

[Learn more](/solutions/kubernetes/) • [Documentation](https://docs.gitlab.com/ee/user/project/clusters/) • [Vision](/direction/configure/kubernetes_configuration/)

## Serverless

Taking full advantage of the power of the cloud computing model and container
orchestration, cloud native is an innovative way to build and run applications.
A big part of our cloud native strategy is around serverless. Serverless
computing provides an easy way to build highly scalable applications and
services, eliminating the pains of provisioning & maintaining.

[Learn more](/product/serverless/) • [Documentation](https://docs.gitlab.com/ee/user/project/clusters/serverless/) • [Vision](/direction/configure/serverless/)

## Runbook Configuration

[Incident Management](https://gitlab.com/groups/gitlab-org/-/epics/349) will
allow operators to have real-time view into the happenings of their systems.
Building upon this concept, we envision rendering of runbook inside of GitLab as
interactive documents for operators which in turn could trigger automation
defined in `gitlab-ci.yml`.

[Documentation](https://docs.gitlab.com/ee/user/project/clusters/runbooks/) • [Vision](/direction/configure/runbooks/)

## ChatOps

The next generation of our ChatOps implementation will allow users to have a
dedicated interface to configure, invoke, and audit ChatOps actions, doing it in
a secure way through RBAC.

[Documenation](https://docs.gitlab.com/ee/ci/chatops/) • [Vision](/direction/configure/chatops/)

## Infrastructure as Code

Infrastructure as code (IaC) is the practice of managing and provisioning infrastructure through
machine-readable definition files, rather than physical hardware configuration or interactive
configuration tools. The IT infrastructure managed by this comprises both physical equipment
such as bare-metal servers as well as virtual machines and associated configuration resources.
The definitions are stored in a version control system. IaC takes proven coding techniques and 
xtends them to your infrastructure directly, effectively blurring the line between what is an
application and what is the environment.

Our focus will be to provide tight integration with best of breed IaC tools, such that all 
infrastructure related workflows in GitLab are well supported. Our initial focus will be on Terraform.

[Vision](/direction/configure/infrastructure_as_code/)

## Chaos Engineering

Chaos engineering in a powerful practice that allows operators to architect
powerful distributed systems that can withstand turbulent conditions. We want
operators to be able to run downtime scenarios randomly to test the resilience
of their architecture. Starting with the minimum units (pods) all the way the
largest units (regions).

[Vision](/direction/configure/chaos_engineering/)

## Cluster Cost Optimization

Compute costs are a significant expenditure for many companies, whether they
are in the cloud or on-premise. Managing these costs is an important function
for many companies. We aim to provide easy-to-understand analysis of your infrastructure
that could identify overprovisioned infrastructure (leading to waste), recommended changes,
estimated costs, and automatic resizing.

[Vision](/direction/configure/cluster_cost_optimization/)

<%= partial("direction/contribute", :locals => { :stageKey => "configure" }) %>

## Prioritization Process

In general, we follow the same [prioritization guidelines](/handbook/product/#prioritization)
as the product team at large. Issues will tend to flow from having no milestone,
to being added to the backlog, to being added to this page and/or a specific
milestone for delivery.

You can see our entire public backlog for Configure at this
[link](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Configure);
filtering by labels or milestones will allow you to explore. If you find
something you're interested in, you're encouraged to jump into the conversation
and participate. At GitLab, everyone can contribute!

Issues with the "direction" label have been flagged as being particularly
interesting, and are listed in the sections below.

## Upcoming Releases

<%= direction %>

<%= partial("direction/other", :locals => { :stage => "configure" }) %>
